import React, { useState } from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import { createBrowserHistory } from "history";
import MicroFrontend from "./MicroFrontend";

import "./App.css";

const defaultHistory = createBrowserHistory();

const {
  REACT_APP_APP1_HOST: app1Host,
  REACT_APP_APP2_HOST: app2Host,
} = process.env;

function App1({ history }) {
  return <MicroFrontend history={history} host={app1Host} name="App1" />;
}

function App2({ history }) {
  return <MicroFrontend history={history} host={app2Host} name="App2" />;
}

function Home({ history }) {
  return (
    <div>
      {/* <Header /> */}
      {/* <div className="home"> */}
        {/* <div className="content"> */}
          <div>
            <App2 />
          </div>
          <div>
            <App1 />
          </div>
          
        {/* </div> */}
      </div>
    // </div>
  );
}

function App({ history = defaultHistory }) {
  return (
    <BrowserRouter>
      <React.Fragment>
        <Switch>
          <Route exact path="/" render={() => <Home history={history} />}/>
        </Switch>
      </React.Fragment>
    </BrowserRouter>
  );
}

export default App;
