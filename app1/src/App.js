import React, { Suspense } from 'react'
import { Canvas, useLoader } from 'react-three-fiber'
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader'
import { OrbitControls } from 'drei'
import './App.css'

function Model() {
  const url = ("model.glb")
  const glb = useLoader(GLTFLoader, url)
  return glb? <primitive object={glb.scene} castShadow /> : null
}

function Cylinder() {
  return (
    <mesh position={[0, -0.3, 0]} rotation-y={-Math.PI / 2} receiveShadow>
      <cylinderGeometry args={[2, 2, 0.5, 60, 20]} />
      <meshStandardMaterial color={'darkgray'} />
    </mesh>
  )
}

export default function App() {
  return (
    <>
    <Canvas shadowMap camera={{ position: [-10, 10, 20], fov: 20 }} style={{ width: `auto`, height: `500px` }}>
      <ambientLight /> 
      <OrbitControls autoRotate={true} />
      <Cylinder/>
      <Suspense fallback={null}>
        <Model />
      </Suspense>
    </Canvas>
    </>
    
  )
}

// function App() {
//   return (
//     <div>
//         <div className="text-center mt-5">
//           <h1>App1</h1>
//         </div>
//         <div className="container">
//           <div className="row mx-5 my-5">
//             <div className="col-md-6">
//                 <div className="card">
//                   <div className="card-body">
//                     <h5 className="card-title">Card 1</h5>
//                     <h6 className="card-subtitle mb-2 text-muted">Card subtitle</h6>
//                     <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
//                     <a href="#" className="card-link">Card link</a>
//                     <a href="#" className="card-link">Another link</a>
//                   </div>
//                 </div>
//             </div>
//             <div className="col-md-6">
//                 <div className="card">
//                   <div className="card-body">
//                     <h5 className="card-title">Card 2</h5>
//                     <h6 className="card-subtitle mb-2 text-muted">Card subtitle</h6>
//                     <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
//                     <a href="#" className="card-link">Card link</a>
//                     <a href="#" className="card-link">Another link</a>
//                   </div>
//                 </div>
//             </div>
//           </div>
//         </div>
//     </div>
//   );
// }

// export default App;
